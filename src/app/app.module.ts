import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {RouterModule, Routes} from '@angular/router';
import {AsyncExampleComponent} from './async-example/async-example.component';
import {StringPaddingExampleComponent} from './string-padding-example/string-padding-example.component';
import {ObjectExampleComponent} from './object-example/object-example.component';
import {TrailingCommaExampleComponent} from './trailing-comma-example/trailing-comma-example.component';
import {CompatibilityListComponent} from './compatibility-list/compatibility-list.component';
import {TaggedTemplateExampleComponent} from './tagged-template-example/tagged-template-example.component';
import {RestSpreadExampleComponent} from './rest-spread-example/rest-spread-example.component';

const appRoutes: Routes = [
  {path: 'compatibility-list', component: CompatibilityListComponent},
  {path: 'async-example', component: AsyncExampleComponent},
  {path: 'string-padding-example', component: StringPaddingExampleComponent},
  {path: 'object-example', component: ObjectExampleComponent},
  {path: 'trailing-comma-example', component: TrailingCommaExampleComponent},
  {path: 'tagged-template-example', component: TaggedTemplateExampleComponent},
  {path: 'rest-spread-example', component: RestSpreadExampleComponent},
];

@NgModule({
  declarations: [
    AppComponent,
    AsyncExampleComponent,
    StringPaddingExampleComponent,
    ObjectExampleComponent,
    TrailingCommaExampleComponent,
    CompatibilityListComponent,
    TaggedTemplateExampleComponent,
    RestSpreadExampleComponent,
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
