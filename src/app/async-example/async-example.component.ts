import {Component, ElementRef, ViewChild} from '@angular/core';

/**
 * For more info check
 *  https://medium.com/@_bengarrison/javascript-es8-introducing-async-await-functions-7a471ec7de8a
 *  https://medium.freecodecamp.org/avoiding-the-async-await-hell-c77a0fb71c4c
 */

@Component({
  selector: 'app-async-example',
  templateUrl: './async-example.component.html',
  styleUrls: ['./async-example.component.css']
})
export class AsyncExampleComponent {

  @ViewChild('textContainer')
  textContainer: ElementRef;

  resolveAfter2Seconds(text: string): Promise<string> {
    return new Promise<string>(resolve => {
      setTimeout(() => {
          console.log('Resolved ' + text);
          resolve(text);
        }, 2000
      );
    });
  }

  async showTextAfter2Seconds(text: string) {
    let promise = this.resolveAfter2Seconds(text);
    this.textContainer.nativeElement.innerHTML = await promise;
  }

  showTextAfter2SecondsWithPromises(text: string): void {
    this.resolveAfter2Seconds(text)
      .then(result => {
        this.textContainer.nativeElement.innerHTML = result;
      });
  }
}
