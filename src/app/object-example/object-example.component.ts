import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-object-example',
  templateUrl: './object-example.component.html',
  styleUrls: ['./object-example.component.css']
})
export class ObjectExampleComponent implements OnInit {

  map: any;

  ngOnInit() {
    this.map = {
      a: 'a-value',
      b: 'b-value',
      c: 'c-value'
    };
  }

  mapValues(): any[] {
    return Object.values(this.map);
  }

  mapEntries(): any[] {
    return Object.entries(this.map);
  }

  propertyDescriptors(): TypedPropertyDescriptor<any> {
    const object = {
      a: 'a',
      get value() {
        return 'x'
      }
    };
    return Object.getOwnPropertyDescriptors(object);
  }

  copyObject(): void {
    const object = {
      a: 'a',
      get value() {
        return 'x'
      },
      set setValue(a: string) {
        this.a = a;
      }
    };
    console.log('Object property descriptors: ' + JSON.stringify(Object.getOwnPropertyDescriptors(object)));

    let newObjectWithoutGettersAndSetters = Object.assign({}, object);
    console.log('Copy (without getters/setters) property descriptors: ' + JSON.stringify(Object.getOwnPropertyDescriptors(newObjectWithoutGettersAndSetters)));

    let newObjectWithGettersAndSetters = Object.defineProperties({}, Object.getOwnPropertyDescriptors(object));
    console.log('Copy (with getters/setters) property descriptors: ' + JSON.stringify(Object.getOwnPropertyDescriptors(newObjectWithGettersAndSetters)));
  }
}
