import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RestSpreadExampleComponent } from './rest-spread-example.component';

describe('RestSpreadExampleComponent', () => {
  let component: RestSpreadExampleComponent;
  let fixture: ComponentFixture<RestSpreadExampleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RestSpreadExampleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RestSpreadExampleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
