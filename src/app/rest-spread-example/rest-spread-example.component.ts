import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-rest-spread-example',
  templateUrl: './rest-spread-example.component.html',
  styleUrls: ['./rest-spread-example.component.css']
})
export class RestSpreadExampleComponent implements OnInit {

  object: any;
  convertedObject: any;

  constructor() {
  }

  ngOnInit() {
    this.object = {
      a: 'a-val',
      b: 'b-val',
      c: 'c-val',
      d: 'd-val',
      nested: {
        e: 'e-val',
        f: 'f-val'
      }
    };
  }

  cloneWithAssign() {
    this.convertedObject = Object.assign({}, this.object);
  }

  cloneWithRest() {
    const {...shallowClone} = this.object;
    this.convertedObject = shallowClone;
  }

  cloneWithSpread() {
    this.convertedObject = {...this.object};
  }

  cloneWithSpreadAndOverride() {
    this.convertedObject = {...this.object, a: 'anotherValue'};
  }

  combineArraysWithSpread() {
    let firstArray = ['a', 'b'];
    let secondArray = ['c', 'd'];
    this.convertedObject = [...firstArray, ...secondArray];
  }

  combineObjectsWithSpread() {
    let firstObject = {a: 'a', b: 'b'};
    let secondObject = {c: 'c'};
    let thirdObject = {nested: {d: 'd'}};
    this.convertedObject = {...firstObject, ...secondObject, ...thirdObject}; // equivalent to Object.assign({ }, firstObject, secondObject, thirdObject);
  }
}
