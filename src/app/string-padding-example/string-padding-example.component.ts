import {Component, ElementRef, ViewChild} from '@angular/core';

@Component({
  selector: 'app-string-padding-example',
  templateUrl: './string-padding-example.component.html',
  styleUrls: ['./string-padding-example.component.css']
})
export class StringPaddingExampleComponent {

  @ViewChild('textContainer')
  textContainer: ElementRef;

  padStart(text: string, length: number): void {
    this.textContainer.nativeElement.innerHTML = text.padStart(length, 'x');
  }

  padEnd(text: string, length: number): void {
    this.textContainer.nativeElement.innerHTML = text.padEnd(length, 'x');
  }
}
