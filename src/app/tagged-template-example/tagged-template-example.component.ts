import {Component} from '@angular/core';

@Component({
  selector: 'app-tagged-template-example',
  templateUrl: './tagged-template-example.component.html',
  styleUrls: ['./tagged-template-example.component.css']
})
export class TaggedTemplateExampleComponent {

  printExampleToConsole() {
    // Tagged literal
    let value = 'some random value';
    console.log(`Value: ${value}`);

    // Try printing function value
    console.log(`Function value: ${() => this.printFunction()}`)

    // Print function value by using tagged template
    console.log(this.functionTemplate `Function value: ${() => this.printFunction()}`);
  }

  printFunction(): string {
    return 'Hello world';
  }

  functionTemplate(literals, func) {
    return literals[0] + func();
  };

  wrapperTemplate(literals, func) {
    return '<div>' + func() + '</div>';
  }
}
