import {Component} from '@angular/core';
import {ObjectExampleComponent} from "../object-example/object-example.component";
import {StringPaddingExampleComponent} from "../string-padding-example/string-padding-example.component";
import {AsyncExampleComponent} from "../async-example/async-example.component";
import {Routes} from "@angular/router";

@Component({
  selector: 'app-trailing-comma-example',
  templateUrl: './trailing-comma-example.component.html',
  styleUrls: ['./trailing-comma-example.component.css']
})
export class TrailingCommaExampleComponent {

  someRandomMethod(param1: string,
                   param2: string,
                   params3: string,) {

  }

}

const appRoutes: Routes = [
  {path: 'async-example', component: AsyncExampleComponent},
  {path: 'string-padding-example', component: StringPaddingExampleComponent},
  {path: 'object-example', component: ObjectExampleComponent},
];
